import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';
import { NewordersService } from '../../../services/neworders.service';
//import {StaffdetailsService} from '../../../services/staffdetails.service';


@Component({
  selector: 'app-assignedorders',
  templateUrl: './assignedorders.component.html',
  styleUrls: ['./assignedorders.component.css']
})
export class AssignedordersComponent implements OnInit {
  order: any = [];

  key:any=[];
  constructor(private ordersSer: NewordersService,private router : Router, private act:ActivatedRoute) { 
  }

  ngOnInit() {
    this.act.queryParams.subscribe(params => {
      console.log(params);
      this.order = params.key;
      this.ordersSer.findByid(this.order).subscribe(success=>{
        this.order=success;
        console.log(this.order);
      },error=>{
        console.log(error);
      })
    });
  }
  getOrder(){
    //console.log("Here too");
    this.ordersSer.findByid(this.order).subscribe(success=>{
      this.order=success;
      console.log(this.order);
    },error=>{
      console.log(error);
    })
  }
  
}
