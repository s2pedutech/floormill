import { TestBed } from '@angular/core/testing';

import { PendingordersService } from './pendingorders.service';

describe('PendingordersService', () => {
  let service: PendingordersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PendingordersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
