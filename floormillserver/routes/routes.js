var app = require('express').Router();
var staffroutes = require('../masters/staff/staff-routes');
var productroutes = require('../masters/product/product-routes');
var producttyperoutes = require('../masters/producttype/producttype-routes');
var ordersroutes = require('../orders/orders-routes');
var customersroutes = require('../customers/customer-routes');

app.use('/staff',staffroutes);
app.use('/product',productroutes);
app.use('/producttype',producttyperoutes);
app.use('/orders',ordersroutes);
app.use('/customers',customersroutes);

module.exports = app;